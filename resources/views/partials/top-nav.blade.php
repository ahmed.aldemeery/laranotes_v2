<h1>{{ $title }}</h1>
<a href="{{ route('home') }}">Home</a> |
<a href="{{ route('logout') }}">Logout</a> |
<a href="{{ route('notes.index') }}">Notes</a> |
<a href="{{ route('categories.index') }}">Categories</a> |
<a href="{{ route('tasks.index') }}">Tasks</a>

<span style="float: right;">
    @yield('links')
</span>
<hr>
