<?php if (!$errors->isEmpty()) { ?>
    <ul>
        <?php foreach ($errors->messages() as $key => $messages) { ?>
            <?php foreach ($messages as $message) { ?>
                <li style="color: red"><?= $message ?></li>
            <?php } ?>
        <?php } ?>
    </ul>
<?php } ?>
