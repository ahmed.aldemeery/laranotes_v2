@extends('layout')

@section('title', 'View category')

@section('links')
<a href="{{ route('categories.edit', ['category' => $category['id']]) }}">Edit</a> |
<form class="delete-form" method="post" action="{{ route('categories.destroy', ['category' => $category['id']]) }}">
    @method('DELETE')
    @csrf()

    <input class="delete-btn" type="submit" value="Delete">
</form>
@endsection
@section('body')
    @include('partials/top-nav', [
        'title' => 'Categories'
    ])
    <div>
        <h3>Name</h3>
        {{ $category['name'] }}

        <h3>Position</h3>
        {{ $category['position'] }}
    </div>
@endsection


