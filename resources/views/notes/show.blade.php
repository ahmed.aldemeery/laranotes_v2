@extends('layout')

@section('title', 'View note')

@section('links')
<a href="{{ route('notes.edit', ['note' => $note['id']]) }}">Edit</a> |
<form class="delete-form" method="post" action="{{ route('notes.destroy', ['note' => $note['id']]) }}">
    @method('DELETE')
    @csrf()

    <input class="delete-btn" type="submit" value="Delete">
</form>
@endsection

@section('body')
    @include('partials/top-nav', [
        'title' => 'Notes'
    ])
    <div>
        <h3>Title</h3>
        <?= $note['title'] ?>

        <h3>Content</h3>
        <?= $note['content'] ?>

        <h3>Position</h3>
        <?= $note['position'] ?>
    </div>
@endsection
