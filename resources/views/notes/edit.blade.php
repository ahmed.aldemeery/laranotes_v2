@extends('layout')

@section('title', 'Edit note')

@section('links')
<a href="{{ route('notes.show', ['note' => $note['id']]) }}">View</a> |
<form class="delete-form" method="post" action="{{ route('notes.destroy', ['note' => $note['id']]) }}">
    @method('DELETE')
    @csrf()

    <input class="delete-btn" type="submit" value="Delete">
</form>
@endsection

@section('body')

@include('partials/top-nav', [
    'title' => 'Notes'
])

@include('partials/errors')

    <form action="<?= route('notes.update', [
        'note' => $note['id'],
    ]) ?>" method="post">
        @method('PUT')
        @csrf()
        <label for="title">Title</label>
        <input type="text" name="title" id="title" value="<?= $note['title'] ?>">

        <select name="categories[]" id="categories" multiple>
        <?php foreach ($categories as $category) {?>
            <?php if (in_array($category['id'], $selected)) { ?>
                <option  value="<?php echo $category['id'];?>" <?= 'selected' ?>><?php echo $category['name'];?></option>
            <?php } else { ?>
                <option  value="<?php echo $category['id'];?>"><?php echo $category['name'];?></option>
            <?php } ?>
        <?php }?>
        </select>

        <label for="title">Content</label>
        <input type="text" name="content" id="content" value="<?= $note['content'] ?>">

        <label for="position">Position</label>
        <input type="number" name="position" id="position" value="<?= $note['position'] ?>">

        <input type="reset" value="Rest" style="float: left;">
        <input type="submit" value="Update">
    </form>
@endsection
