@extends('layout')

@section('title', 'Create Note')

@section('body')

@include('partials/top-nav', [
    'title' => 'Notes'
])

@include('partials/errors')


    <form action="<?= route('notes.store') ?>" method="post">
        @csrf
        <label for="Title">Title</label>
        <input type="text" name="title" id="title">

        <select name="categories[]" id="categories" multiple>
        <?php foreach ($categories as $category) {?>
            <option  value="<?php echo $category['id'];?>" ><?php echo $category['name'];?></option>
        <?php }?>
        </select>

        <label for="content">Content</label>
        <input type="text" name="content" id="content" width="80" height="100" >

        <label for="position">Position</label>
        <input type="number" name="position" id="position">

        <input type="reset" value="Rest" style="float: left;">
        <input type="submit" value="Create">
    </form>
@endsection
