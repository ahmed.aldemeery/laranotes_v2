<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.css">
</head>
<body>
    <h1>Home</h1>
    <a href="{{ route('logout') }}">Logout</a> |
    <a href="{{ route('notes.index') }}">Notes</a> |
    <a href="{{ route('categories.index') }}">Categories</a> |
    <a href="{{ route('tasks.index') }}">Tasks</a>
    <hr>
</body>
</html>
