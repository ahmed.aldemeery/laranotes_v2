@extends('layout')

@section('title', 'Edit Task')

@section('links')
<a href="{{ route('tasks.show', ['task' => $task['id']]) }}">View</a> |
<form class="delete-form" method="post" action="{{ route('tasks.destroy', ['task' => $task['id']]) }}">
    @method('DELETE')
    @csrf()

    <input class="delete-btn" type="submit" value="Delete">
</form>
@endsection

@section('body')

@include('partials/top-nav', [
    'title' => 'Tasks'
])

@include('partials/errors')

<form action="{{ route('tasks.update', ['task' => $task['id']]) }}" method="post">
    @method('PUT')
    @csrf()
    <label for="name">Name</label>
    <input type="text" name="name" id="name" value="{{ $task['name'] }}">

    <label for="position">Position</label>
    <input type="number" name="position" id="position" value="{{ $task['position'] }}">

    <label for="done">Done</label>
    <input type="checkbox" name="done" id="done" value="{{ $task['done'] }}" {{ $task['done'] ? 'checked' : '' }}>

    <br />
    <br />

    <input type="submit" value="Update" style="float: left;">
    <input type="reset" value="Reset" >
</form>

@endsection
