@extends('layout')

@section('title', 'View Task')

@section('links')
<a href="{{ route('tasks.edit', ['task' => $task['id']]) }}">Edit</a> |
<form class="delete-form" method="post" action="{{ route('tasks.destroy', ['task' => $task['id']]) }}">
    @method('DELETE')
    @csrf()

    <input class="delete-btn" type="submit" value="Delete">
</form>
@endsection

@section('body')
    @include('partials/top-nav', [
        'title' => 'Tasks'
    ])
    <div>
        <h3>Name</h3>
        {{ $task['name'] }}

        <h3>Position</h3>
        {{ $task['position'] }}

        <h3>Done</h3>
        {{ $task['done'] ? '✅' : '❌' }}

    </div>
@endsection
