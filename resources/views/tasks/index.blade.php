@extends('layout')

@section('title', 'All Tasks')

@section('links')
<a href="{{ route('tasks.create') }}">New Task</a>
@endsection

@section('body')
    @include('partials/top-nav', [
        'title' => 'Tasks'
    ])


    <table>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Position</th>
            <th>Done</th>
            <th>Actions</th>
        </tr>
        @foreach ($tasks as $task)
        <tr>
            <td>{{ $task['id'] }}</td>
            <td>{{ $task['name'] }}</td>
            <td>{{ $task['position'] }}</td>
            <td>{{ $task['done'] ? '✅' : '❌' }}</td>
            <td>
                <a href="<?= route('tasks.edit', [
                    'task' => $task['id']
                ]) ?>">Edit</a> |
                <a href="<?= route('tasks.show', [
                    'task' => $task['id']
                ]) ?>">View</a> |

                <form class="delete-form" method="post" action="<?= route('tasks.destroy', [
                    'task' => $task['id']
                ]) ?>">
                    @method('DELETE')
                    @csrf()

                    <input class="delete-btn" type="submit" value="Delete">
                </form>
            </td>
        </tr>
        @endforeach

    </table>

    {{ $tasks->links() }}
@endsection
