<?php

namespace App\Http\Controllers;

use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{


    public function index()
    {
        $user = Auth::user();

        $tasks = $user->tasks()->orderBy('position')->paginate(10);

        return view('tasks/index', [
            'tasks' => $tasks,
        ]);
    }

    public function show(Task $task)
    {
        return view('tasks/show', [
            'task' => $task,
        ]);
    }

    public function create()
    {
            return view('tasks/create');
    }

    public function store(Request $request )
    {
        $user = Auth::user();

        $data = $request->validate([
            'name' => [
                'required',
                Rule::unique('tasks')->where(function ($query) use ($user) {
                    return $query->where('user_id', $user->id);
                })
            ],
            'position' => 'required|integer',
        ]);

        $data['done'] = $request->has('done');

        $user->tasks()->create($data);

        return to_route('tasks.index');
    }

    public function edit(Task $task)
    {
        return view('tasks/edit', [
            'task' => $task,
        ]);
    }

    public function update(Request $request, Task $task)
    {
        $user = Auth::user();

        $data = $request->validate([
            'name' => [
                'required',
                Rule::unique('tasks')->where(function ($query) use ($user) {
                    return $query->where('user_id', $user->id);
                })->ignore($task->id),
            ],
            'position' => 'required|integer',
        ]);

        $task['done'] = $request->has('done');

        $task->update($data);

        return to_route('tasks.index');
    }

    public function destroy(Task $task)
    {
        $task->delete();

        return to_route('tasks.index');
    }

}





