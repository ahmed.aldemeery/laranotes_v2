<?php

namespace App\Http\Controllers;

use App\Models\Note;
use App\Models\User;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreNoteRequest;

class NoteController extends Controller
{


    public function index()
    {
        $user = Auth::user();

        $notes = $user->notes()->orderBy('position')->get();

        return view('notes/index', [
            'notes' => $notes,
        ]);
    }

    public function show(Note $note)
    {
        return view('notes/show', [
            'note' => $note,
        ]);
    }

    public function create()
    {
        $user = Auth::user();

        $categories = $user->categories()->get();

        return view('notes/create', [
            'categories' => $categories,
        ]);
    }

    public function store(Request $request)
    {
        $user = Auth::user();

        $data = $request -> validate([
            'title' => [
                'required',
                Rule::unique('notes')->where(function ($query) use ($user) {
                    return $query->where('user_id', $user->id);
                })
            ],
            'position' => [
                'required',
                'integer',
            ],

            'content' => [
                'required',
            ],
            'categories' => [
                'required',
                'array',
            ],
            'categories.*' => 'exists:categories,id',
        ]);

        $note = $user->notes()->create($data);
        $note->categories()->attach($data['categories']);

        return to_route('notes.index');
    }

    public function edit(Note $note)
    {
        $user = Auth::user();

        $categories = $user->categories()->get();
        $selected = $note->categories()->pluck('categories.id')->toArray();

        return view('notes/edit', [
            'note' => $note,
            'categories' => $categories,
            'selected' => $selected,
        ]);
    }

    public function update(Request $request, Note $note)
    {
        $user = Auth::user();

        $data = $request->validate([
            'title' => [
                'required',
                Rule::unique('notes')->where(function ($query) use ($user) {
                    return $query->where('user_id', $user->id);
                })->ignore($note->id),
            ],
            'position' => 'required|integer',
            'content' => [
                'required',
            ],
            'categories' => [
                'required',
                'array',
            ],
            'categories.*' => 'exists:categories,id',
        ]);

        $note->update($data);
        $note->categories()->sync($data['categories']);

        return to_route('notes.index');
    }

    public function destroy(Note $note)
    {
        $note->delete();

        return to_route('notes.index');
    }
}


