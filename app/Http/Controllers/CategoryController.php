<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Category;
use Illuminate\Http\Request;

use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{


    public function index()
    {
        $user = Auth::user();

        $categories = $user->categories()->orderBy('position')->get();

        return view('categories/index', [
            'categories' => $categories,
        ]);
    }

    public function show(Category $category)
    {
        return view('categories/show', [
            'category' => $category
        ]);
    }

    public function create()
    {
        return view('categories/create');
    }

    public function store(request $request)
    {
        $user = Auth::user();

        $data = $request->validate([
            'name' => [
                'required',
                Rule::unique('categories')->where(function ($query) use ($user) {
                    return $query->where('user_id', $user->id);
                })
            ],
            'position' => [
                'required',
                'integer',
            ],
        ]);

        $user->categories()->create($data);

        return to_route('categories.index');
    }

    public function edit(Category $category)
    {
        return view('categories/edit', [
            'category' => $category
        ]);
    }

    public function update(Request $request, Category $category)
    {
        $user = Auth::user();

        $data = $request->validate([
            'name' => [
                'required',
                Rule::unique('categories')->where(function ($query) use ($user) {
                    return $query->where('user_id', $user->id);
                })->ignore($category->id),
            ],
            'position' => 'required|integer',
        ]);

        $category->update($data);

        return to_route('categories.index');
    }

    public function destroy(Category $category)
    {
        $category->delete();

        return to_route('categories.index');
    }
}



