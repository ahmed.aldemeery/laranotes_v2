<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'name',
        'position',
        'done',
    ];

    protected $casts = [
        'done' => 'boolean',
    ];

    public function markAsDone(): void
    {
        $this->done = true;
        $this->save();
    }

    public function markAsUndone(): void
    {
        $this->update(['done' => false]);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
